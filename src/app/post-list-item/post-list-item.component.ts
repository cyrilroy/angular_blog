import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number = 0;
  @Input() created_at: Date

  constructor() {
    this.created_at = new Date();
  }

  ngOnInit() {
  }

  onLove() {
    this.loveIts++;
  }
  
  onNotLove() {
    this.loveIts--;
  }
}
